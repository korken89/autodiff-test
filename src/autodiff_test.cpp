#include <iostream>
#include <Eigen/Dense>
#include "autodiff_test/ADJacobian.h"

/*
 * Testing differentiation that will produce a gradient.
 */
template <typename Scalar>
Scalar scalar_func(const Eigen::Matrix<Scalar, 3, 1> &input,
                   Eigen::Matrix<Scalar, 3, 1> *grad)
{
  eigen_assert(grad != 0);

  /* Some typedefs to not need and rewrite the long expressions. */
  typedef Eigen::AutoDiffScalar< Eigen::Matrix<Scalar, input.RowsAtCompileTime, 1> > ADS;

  /* Create and initialize the AutoDiff vector. */
  Eigen::Matrix<ADS, input.RowsAtCompileTime, 1> ad;

  for (int i = 0; i < input.RowsAtCompileTime; i++)
    ad(i) = ADS(input(i), input.RowsAtCompileTime, i);  // AutoDiff initialization

  ADS s(0);

  for (int i = 0; i < input.RowsAtCompileTime; i++)
  {
    s += exp(ad(i));
  }

  (*grad) = s.derivatives();

  return s.value();
}

/*
 * Testing differentiation that will produce a Jacobian, using functors and the
 * ADJacobian helper.
 *
 * Example: 2 state 0th order integrator
 */
template <typename Scalar>
struct integratorFunctor
{
  /*
   * Definitions required by ADJacobian.
   */
  typedef Eigen::Matrix<Scalar, 2, 1> InputType;
  typedef Eigen::Matrix<Scalar, 2, 1> ValueType;

  /*
   * Implementation starts here.
   */
  integratorFunctor(const Scalar gain) : _gain(gain) {}
  const Scalar _gain;

  /* The types of the arguments are inferred by ADJacobian.
   * For ease of thinking, T1 = InputType and T2 = ValueType. */
  template <typename T1, typename T2>
  void operator() (const T1 &input, T2 *output, const Scalar dt) const
  {
    T2 &o = *output;

    /* Integrator to test the AD. */
    o[0] = input[0] + input[1] * dt * _gain;
    o[1] = input[1] * _gain;
  }
};


int main(int argc, char *argv[])
{

  typedef Eigen::Matrix<float, 3, 1> myvec;

  /* Value vector for the gradient test. */
  myvec vec, vec_grad;
  vec << 1,2,3;

  /*
   * Run the test using AutoDiffScalar.
   */
  auto grad_test = scalar_func(vec, &vec_grad);


  /*
   * Run the example using ADJacobian.
   */

  /* Input vector and sampling time. */
  Eigen::Matrix<float, 2, 1> in;
  in << 1,2;
  const float dt = 1e-2;
  const float gain = 3;

  /* Outputs. */
  Eigen::Matrix<float, 2, 1> out;
  Eigen::Matrix<float, 2, 2> jac;

  /* Test the ADJacobian. */
  Eigen::ADJacobian< integratorFunctor<float> > adjac(3);
  adjac(in, &out, &jac, dt);


  /*
   * Do some printing.
   */
  std::cout << "grad_test = " << std::endl;
  std::cout << grad_test << std::endl << std::endl;
  std::cout << "Gradient of grad_test = " << std::endl;
  std::cout << vec_grad << std::endl << std::endl;

  std::cout << std::endl << "ADJacobian test on 0th order integrator, dt = "
            << dt << std::endl;
  std::cout << "in = " << std::endl;
  std::cout << in << std::endl << std::endl;
  std::cout << "out = " << std::endl;
  std::cout << out << std::endl << std::endl;
  std::cout << "Jacobian = " << std::endl;
  std::cout << jac << std::endl;

  return 0;
}

